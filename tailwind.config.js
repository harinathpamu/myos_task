module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        primary: "#76a9ff",
        "primary-dark": "#3d7acb",
      },
    },
  },
  plugins: [require("@tailwindcss/line-clamp")],
};
