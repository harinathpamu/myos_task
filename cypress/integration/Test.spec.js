/// <reference types="cypress" />

describe("Shopping", () => {
  it("Displays 20 product items by default", () => {
    cy.visit("http://localhost:3000/");
    cy.get('[data-cy="carditem"]').should("have.length", 20);
  });

  it("Navigates to cart page from home page", () => {
    cy.visit("http://localhost:3000/");
    cy.get('[data-cy="cartlink"]').click();
    cy.location("pathname").should("include", "/cart");
  });

  it("Adding item to cart", () => {
    cy.visit("http://localhost:3000/");
    cy.get('[data-cy="addtocart"]').first().click();
    cy.get('[data-cy="gotocart"]')
      .first()
      .contains(/Go to cart/i);
    cy.get('[data-cy="cartsize"').contains("1");
  });

  it('Navigates to cart page by clicking on "go to cart" in cart item', () => {
    cy.visit("http://localhost:3000/");
    cy.get('[data-cy="addtocart"]').first().click();
    cy.get('[data-cy="gotocart"]').first().click();
    cy.location("pathname").should("include", "/cart");
  });

  it("Create order", () => {
    cy.visit("http://localhost:3000/");
    cy.get('[data-cy="addtocart"]').first().click();
    cy.get('[data-cy="gotocart"]').first().click();
    cy.location("pathname").should("include", "/cart");
    cy.get('[data-cy="email"]').type("testing@gmail.com");
    cy.get('[data-cy="completeorder"]').click();
    cy.get('[data-cy="ordermail"]').first().contains("testing@gmail.com");
  });

  it('Navigates to home page by clicking on "order more" in cart page', () => {
    cy.visit("http://localhost:3000/");
    cy.get('[data-cy="addtocart"]').first().click();
    cy.get('[data-cy="gotocart"]').first().click();
    cy.location("pathname").should("include", "/cart");
    cy.get('[data-cy="email"]').type("testing@gmail.com");
    cy.get('[data-cy="completeorder"]').click();
    cy.get('[data-cy="ordermore"]').first().click();
    cy.location("pathname").should("include", "/");
  });
});
