export interface Product {
  id: number;
  title: string;
  price: number;
  description: string;
  category: string;
  image: string;
  rating: {
    rate: number;
  };
}

export interface Order {
  email: string;
  totalAmount: number;
  items: Product[];
}
