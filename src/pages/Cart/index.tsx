import { useContext, useMemo } from "react";

import { Link } from "react-router-dom";

import { Product } from "../../shared/types";
import Header from "../../common-components/Header";
import { CartContext } from "../../contexts/CartContext";
import CompletedOrder from "../../common-components/CompletedOrder";
import CartProductItem from "../../common-components/CartProductItem";

function Cart() {
  const { cart, removeFromCart, orders, addOrder, clearCart } =
    useContext(CartContext);

  const totalAmount = useMemo(() => {
    if (cart && cart.length > 0) {
      return cart
        .reduce((acc, it) => acc + Number(it?.price.toFixed(2)), 0)
        .toFixed(2);
    } else {
      return 0;
    }
  }, [cart]);

  const onRemoveHandler = (product: Product) => {
    removeFromCart(product);
  };

  const onAddOrderHandler = (
    cart: Product[],
    totalAmount: number,
    email: string
  ) => {
    if (cart && cart.length > 0 && totalAmount && email) {
      clearCart();
      addOrder({ items: cart, totalAmount, email });
    }
  };

  return (
    <div>
      <Header />
      {orders && orders?.length <= 0 && cart && cart?.length <= 0 && (
        <div className="p-20 flex flex-col items-center justify-center">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-64 w-64 text-gray-200"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            role="button"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={1}
              d="M16 11V7a4 4 0 00-8 0v4M5 9h14l1 12H4L5 9z"
            />
          </svg>
          <h1 className="text-gray-400 text-lg">No Orders</h1>
        </div>
      )}
      <div className="container p-4 mx-auto space-y-4 select-none">
        {cart && cart?.length > 0 && (
          <div className="p-4 rounded-lg border max-w-3xl mx-auto space-y-2">
            <h1 className="font-bold text-lg">Cart ({cart?.length ?? 0})</h1>
            {cart?.map((it) => {
              return (
                <CartProductItem
                  key={it.id}
                  product={it}
                  onRemoveItem={onRemoveHandler}
                />
              );
            })}
            <div className="mt-5 space-y-4">
              <div className="flex justify-between border-t pt-4">
                <p className="text-lg font-bold text-gray-900">Total Amount</p>
                <p className="text-lg font-bold text-gray-900">
                  € {totalAmount}
                </p>
              </div>
              <form
                onSubmit={(event) => {
                  event.preventDefault();
                  const target = event.target as typeof event.target & {
                    email: { value: string };
                  };
                  onAddOrderHandler(cart, +totalAmount, target.email.value);
                }}
                className="flex flex-col gap-2 md:flex-row md:justify-between border-t pt-4"
              >
                <input
                  id="email"
                  type="email"
                  placeholder="Email"
                  data-cy="email"
                  required
                  className="bg-gray-100 rounded-lg focus:outline-none px-4 py-3.5 text-gray-600 text-sm font-light w-full md:max-w-sm"
                />
                <div>
                  <button
                    data-cy="completeorder"
                    type="submit"
                    className="block btn btn-primary mx-auto"
                  >
                    Complete Order
                  </button>
                </div>
              </form>
            </div>
          </div>
        )}
        {orders && orders?.length > 0 && (
          <div className="mx-auto max-w-3xl space-y-4">
            <h1 className="text-lg text-gray-700 font-bold">All Orders</h1>
            {orders?.map((order, index) => {
              return (
                <CompletedOrder key={index} id={index + 1} order={order} />
              );
            })}
            <div className="flex justify-center">
              {orders && orders?.length > 0 && cart && cart?.length <= 0 && (
                <Link
                  data-cy="ordermore"
                  to="/"
                  className="inline-block mx-auto btn btn-primary"
                >
                  Order more
                </Link>
              )}
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

export default Cart;
