import { useContext, useState } from "react";

import { useQuery } from "react-query";

import { Product } from "../../shared/types";
import Header from "../../common-components/Header";
import Search from "../../common-components/Search";
import { CartContext } from "../../contexts/CartContext";
import ProductItem from "../../common-components/ProductItem";

function Home() {
  const { cart, addToCart } = useContext(CartContext);
  const [search, setSearch] = useState<string>("");
  const { data, isLoading, isError, refetch } = useQuery<Product[]>(
    "products",
    () =>
      fetch("/products.json")
        .then((res) => res.json())
        .then((json) => json)
  );

  if (isLoading) {
    return <span>Loading...</span>;
  }

  if (isError) {
    return (
      <span>
        Something went wrong : ({" "}
        <span
          className="no-underline hover:underline text-blue-500"
          onClick={() => refetch()}
          role="button"
        >
          Retry
        </span>
      </span>
    );
  }

  const onAddToCartHandler = (product: Product) => {
    addToCart(product);
  };

  const hasAddedToCartHandler = (product: Product, cart: Product[]) => {
    if (product && cart && cart?.length > 0) {
      const found = cart?.find((it) => it?.id === product?.id);
      if (found) {
        return true;
      } else {
        return false;
      }
    }
    return false;
  };

  return (
    <>
      <Header />
      <div className="container mx-auto p-2 md:p-6  space-y-4">
        <Search value={search} onChangeSearch={(value) => setSearch(value)} />
        <div className="grid place-items-center  grid-cols-[repeat(auto-fit,minmax(280px,1fr))] gap-x-1 gap-y-4">
          {data
            ?.filter(
              (it) =>
                it?.title?.trim().includes(search?.trim()) ||
                it?.description?.trim().includes(search?.trim())
            )
            ?.map((it) => {
              return (
                <ProductItem
                  data={it}
                  key={it?.id}
                  onAddToCart={onAddToCartHandler}
                  hasAddedToCart={hasAddedToCartHandler(it, cart)}
                />
              );
            })}
        </div>
      </div>
    </>
  );
}

export default Home;
