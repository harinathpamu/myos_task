import { createContext } from "react";

import { Product, Order } from "../shared/types";

interface CartContextType {
  cart: Product[];
  orders: Order[];
  addToCart: (it: Product) => void;
  removeFromCart: (it: Product) => void;
  addOrder: (order: Order) => void;
  clearCart: () => void;
}

export const CartContext = createContext<CartContextType>({
  cart: [],
  orders: [],
  addToCart: () => {},
  removeFromCart: () => {},
  addOrder: () => {},
  clearCart: () => {},
});
