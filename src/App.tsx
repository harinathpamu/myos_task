import { useCallback, useMemo, useState, lazy, Suspense } from "react";

import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

import { Product, Order } from "./shared/types";
import { CartContext } from "./contexts/CartContext";

const Home = lazy(() => import("./pages/Home"));
const Cart = lazy(() => import("./pages/Cart"));

function App() {
  const [cart, setCart] = useState<Product[]>([]);
  const [orders, setOrders] = useState<Order[]>([]);

  const onAddToCartHandler = useCallback((product: Product) => {
    setCart((state) => [...state, product]);
  }, []);

  const onRemoveFromCartHandler = useCallback(
    (product: Product) => {
      const filtered = cart?.filter((it) => it?.id !== product?.id);
      setCart(filtered);
    },
    [cart]
  );

  const addOrder = useCallback((order: Order) => {
    setOrders((state) => [...state, order]);
  }, []);

  const clearCart = useCallback(() => {
    setCart([]);
  }, []);

  const providerValue = useMemo(() => {
    return {
      cart,
      addToCart: onAddToCartHandler,
      removeFromCart: onRemoveFromCartHandler,
      orders,
      addOrder,
      clearCart,
    };
  }, [
    cart,
    orders,
    onAddToCartHandler,
    onRemoveFromCartHandler,
    addOrder,
    clearCart,
  ]);

  return (
    <Router>
      <CartContext.Provider value={providerValue}>
        <Routes>
          <Route
            caseSensitive
            path="/"
            element={
              <Suspense fallback={<div>Loading...</div>}>
                <Home />
              </Suspense>
            }
          />
          <Route
            path="/cart"
            element={
              <Suspense fallback={<div>Loading...</div>}>
                <Cart />
              </Suspense>
            }
          />
        </Routes>
      </CartContext.Provider>
    </Router>
  );
}

export default App;
