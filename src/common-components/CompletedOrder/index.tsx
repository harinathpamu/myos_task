import { useState } from "react";

import { Product, Order } from "../../shared/types";
import CartProductItem from "../../common-components/CartProductItem";

interface CompletedOrderProps {
  order: Order;
  id: number;
}

function CompletedOrder(props: CompletedOrderProps) {
  const { order, id } = props;
  const [show, setShow] = useState(false);
  return (
    <div className="p-4 rounded-lg border max-w-3xl mx-auto space-y-2">
      <h1>
        <div className="flex justify-between items-center">
          <div className="flex flex-col">
            <div className="flex items-center gap-2">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className={`h-6 w-6 text-green-500 `}
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                role="button"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={1}
                  d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"
                />
              </svg>
              <span className="font-semibold text-green-500 text-lg">
                Order #{id} Placed <span className="text-xs font-thin">by</span>{" "}
              </span>
            </div>
            <p
              data-cy="ordermail"
              className="text-sm font-semibold text-green-500 "
            >
              {order?.email}
            </p>
          </div>
          <span>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className={`h-6 w-6 text-primary hover:text-primary-dark ${
                show ? "rotate-0" : "-rotate-90"
              }`}
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              role="button"
              onClick={() => setShow(!show)}
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M19 9l-7 7-7-7"
              />
            </svg>
          </span>
        </div>
      </h1>
      {show && (
        <div className="space-y-2">
          {order?.items?.map((it: Product) => {
            return <CartProductItem key={it?.id} product={it} />;
          })}
          <div className="mt-5 space-y-4">
            <div className="flex justify-between border-t pt-4">
              <p className="text-lg font-bold text-gray-900">
                Total Amount Paid
              </p>
              <p className="text-lg font-bold text-gray-900">
                € {order?.totalAmount}
              </p>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

export default CompletedOrder;
