import { useContext } from "react";

import { Link } from "react-router-dom";

import { CartContext } from "../../contexts/CartContext";

function Header() {
  const { cart } = useContext(CartContext);

  return (
    <header className="px-6 py-3.5 border-b shadow-sm flex justify-between items-center">
      <Link to="/">
        <p className="text-lg font-bold text-primary uppercase">
          Myos Shopping
        </p>
      </Link>
      <div>
        <Link data-cy="cartlink" to="/cart" className="relative">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-6 w-6 text-primary hover:text-primary-dark"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            role="button"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={1}
              d="M16 11V7a4 4 0 00-8 0v4M5 9h14l1 12H4L5 9z"
            />
          </svg>
          {cart && cart?.length > 0 && (
            <span
              data-cy="cartsize"
              className="p-1 flex items-center h-4 text-white justify-center bg-red-500 rounded-full text-xs absolute  -right-8 top-0"
            >
              {cart?.length}
            </span>
          )}
        </Link>
      </div>
    </header>
  );
}

export default Header;
