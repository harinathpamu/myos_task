interface SearchProps {
  value: string;
  onChangeSearch?: (val: string) => void;
}

function Search(props: SearchProps) {
  const { value, onChangeSearch } = props;

  return (
    <div className="rounded-xl px-4 bg-gray-100 flex gap-2 max-w-3xl mx-auto">
      <span className="py-4">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="h-6 w-6 text-primary hover:text-primary-dark"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={1}
            d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
          />
        </svg>
      </span>
      <input
        value={value ?? ""}
        onChange={(e) => onChangeSearch?.(e.target.value)}
        placeholder="Search"
        className="bg-transparent h-auto flex-1 text-gray-600 text-sm font-light focus:outline-none"
      />
      {value && (
        <span className="py-4" onClick={() => onChangeSearch?.("")}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-6 w-6 text-primary hover:text-primary-dark"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            role="button"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={1}
              d="M6 18L18 6M6 6l12 12"
            />
          </svg>
        </span>
      )}
    </div>
  );
}

export default Search;
