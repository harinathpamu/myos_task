import { Product } from "../../shared/types";

interface CartItemProps {
  product: Product;
  onRemoveItem?: (product: Product) => void;
}

function CartProductItem(props: CartItemProps) {
  const { product, onRemoveItem } = props;
  return (
    <div className="flex items-center justify-between hover:shadow px-6 rounded-lg group bg-white">
      <div className="flex gap-5 my-2">
        <img
          alt="product"
          className="w-16 max-w-full bg-transparent"
          src={product?.image}
        />
        <div className="flex flex-col justify-center py-2">
          <p className="text-sm font-semibold leading-tight text-gray-800">
            {product?.title}
          </p>
          <span className="flex items-center gap-1 text-xs ">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-4 w-4 text-yellow-500"
              fill="currentColor"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={1}
                d="M11.049 2.927c.3-.921 1.603-.921 1.902 0l1.519 4.674a1 1 0 00.95.69h4.915c.969 0 1.371 1.24.588 1.81l-3.976 2.888a1 1 0 00-.363 1.118l1.518 4.674c.3.922-.755 1.688-1.538 1.118l-3.976-2.888a1 1 0 00-1.176 0l-3.976 2.888c-.783.57-1.838-.197-1.538-1.118l1.518-4.674a1 1 0 00-.363-1.118l-3.976-2.888c-.784-.57-.38-1.81.588-1.81h4.914a1 1 0 00.951-.69l1.519-4.674z"
              />
            </svg>
            <span className="text-gray-800">{product?.rating?.rate}</span>
          </span>
          <div className="flex flex-col ">
            <span className="text-gray-800 text-lg font-bold">
              € {product?.price?.toFixed(2)}
            </span>
          </div>
        </div>
      </div>
      {onRemoveItem && (
        <span
          className="hidden group-hover:block"
          onClick={() => onRemoveItem?.(product)}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-6 w-6 text-primary hover:text-primary-dark"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            role="button"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={1}
              d="M6 18L18 6M6 6l12 12"
            />
          </svg>
        </span>
      )}
    </div>
  );
}

export default CartProductItem;
