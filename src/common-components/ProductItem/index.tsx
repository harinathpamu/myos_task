import { Link } from "react-router-dom";

import { Product } from "../../shared/types";

interface ProductItemProps {
  data: Product;
  onAddToCart: (val: Product) => void;
  hasAddedToCart: boolean;
}

function ProductItem(props: ProductItemProps) {
  const { data, onAddToCart, hasAddedToCart } = props;
  return (
    <div
      data-cy="carditem"
      className="flex flex-col border rounded-xl p-5 max-w-[280px]  space-y-4 "
    >
      <div className="flex-1">
        <img
          alt={data?.title}
          src={data?.image}
          className="block h-72 w-full max-w-full"
        />
      </div>
      <div className="space-y-2">
        <div className="space-y-1">
          <p className="text-sm font-semibold leading-tight text-gray-800 line-clamp-1">
            {data?.title}
          </p>
          <p className="text-xs font-thin  leading-tight text-gray-800 line-clamp-2">
            {data?.description}
          </p>
        </div>
        <span className="flex items-center gap-1 text-xs ">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-4 w-4 text-yellow-500"
            fill="currentColor"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={1}
              d="M11.049 2.927c.3-.921 1.603-.921 1.902 0l1.519 4.674a1 1 0 00.95.69h4.915c.969 0 1.371 1.24.588 1.81l-3.976 2.888a1 1 0 00-.363 1.118l1.518 4.674c.3.922-.755 1.688-1.538 1.118l-3.976-2.888a1 1 0 00-1.176 0l-3.976 2.888c-.783.57-1.838-.197-1.538-1.118l1.518-4.674a1 1 0 00-.363-1.118l-3.976-2.888c-.784-.57-.38-1.81.588-1.81h4.914a1 1 0 00.951-.69l1.519-4.674z"
            />
          </svg>
          <span className="text-gray-800">{data?.rating?.rate}</span>
        </span>
        <div className="flex justify-between items-center">
          <div className="flex flex-col ">
            <span className="text-gray-400 text-xs ">Price</span>
            <span className="text-gray-800 text-lg font-semibold">
              € {data?.price?.toFixed(2)}
            </span>
          </div>
          <div>
            {hasAddedToCart ? (
              <Link
                data-cy="gotocart"
                to={"/cart"}
                className="btn btn-success inline-block"
              >
                Go to Cart
              </Link>
            ) : (
              <button
                data-cy="addtocart"
                onClick={() => onAddToCart?.(data)}
                className="btn btn-primary "
              >
                Add to Cart
              </button>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default ProductItem;
